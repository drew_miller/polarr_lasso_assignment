"use strict";

let glCanvas = document.getElementById("editor_canvas");

// The lasso canvas doesn't need to be visible, but I show it here so you can see how it works.
// let lassoCanvas = document.createElement("canvas");
let lassoCanvas = document.getElementById("lasso_canvas");

lassoCanvas.width = glCanvas.width;
lassoCanvas.height = glCanvas.height;
let lassoContext = lassoCanvas.getContext("2d");
let gl = glCanvas.getContext("webgl");
let brightnessInput = document.getElementById("brightness_input");
let brightnessMultiplier = 0.25;

gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

function updateBrightness() {
    brightnessMultiplier = brightnessInput.value;
    updateScene();
}

function initImageVs() {
    let vsCode = `
attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
varying highp vec2 vTextureCoord;

void main(void) {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    vTextureCoord = aTextureCoord;
}
`;
    let vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsCode);
    gl.compileShader(vs);
    return vs;
}
let imageVs = initImageVs();

function initImageFs() {
    let imageFsCode = `
precision highp float;

varying highp vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform sampler2D uLasso;

uniform float uBrightness;

void main(void) {
    gl_FragColor = texture2D(uSampler, vTextureCoord);
    gl_FragColor.rgb += uBrightness * texture2D(uLasso, vTextureCoord).a;
}
`;
    let imageFs = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(imageFs, imageFsCode);
    gl.compileShader(imageFs);
    return imageFs;
}
let imageFs = initImageFs();

function initFillFs() {
    let fillFsCode = `
void main(void) {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1);
}
`;
    let fillFs = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fillFs, fillFsCode);
    gl.compileShader(fillFs);
    return fillFs;
}
let fillFs = initFillFs();

let imageProgram = gl.createProgram();
gl.attachShader(imageProgram, imageVs);
gl.attachShader(imageProgram, imageFs);
gl.linkProgram(imageProgram);
gl.useProgram(imageProgram);

gl.clearColor(0, 0, 0, 1);
gl.viewport(0, 0, glCanvas.width, glCanvas.height);

function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}

function closeEnoughToCompleteLasso(lassoCoordinates) {
    let start = lassoCoordinates[0];
    let end = lassoCoordinates[lassoCoordinates.length - 1];
    let distanceSquared = Math.pow(end.y - start.y, 2) + Math.pow(end.x - start.x, 2);
    return distanceSquared <= Math.pow(100, 2);
}

function bindTextureFromImage(texture, image) {
    const level = 0;
    const internalFormat = gl.RGBA;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;

    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image);

    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
        gl.generateMipmap(gl.TEXTURE_2D);
    } else {
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
}

function resizeCanvasesToImage(image) {
    let width = 640;
    let height = (width * image.height / image.width) || width;

    lassoCanvas.width = width;
    lassoCanvas.height = height;
    lassoCanvas.style.width = width + "px";
    lassoCanvas.style.height = height + "px";
    glCanvas.width = width;
    glCanvas.height = height;
    glCanvas.style.width = width + "px";
    glCanvas.style.height = height + "px";
    gl.viewport(0, 0, width, height);
}

function loadUrlToTexture(gl, texture, url) {
    gl.bindTexture(gl.TEXTURE_2D, texture);

    const level = 0;
    const internalFormat = gl.RGBA;
    const width = 1;
    const height = 1;
    const border = 0;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;
    const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
    gl.texImage2D(
        gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, pixel
    );

    const image = new Image();
    image.onload = function () {
        resizeCanvasesToImage(image);
        bindTextureFromImage(texture, image);
        if (updateScene) {
            updateScene()
        }
    };
    image.src = url;

    return texture;
}

const imageTexture = gl.createTexture();
loadUrlToTexture(gl, imageTexture, "brainalyzer.png");

const lassoTexture = gl.createTexture();

function loadFile(event) {
    if (event.target.files.length) {
        if (!event.target.files[0].type.startsWith("image/")) {
            window.alert("Not an image: " + event.target.files[0].name);
            return;
        }
        let reader = new FileReader();
        reader.onload = function () {
            let image = new Image();
            image.src = reader.result;
            image.onload = function() {
                resizeCanvasesToImage(image);
                bindTextureFromImage(imageTexture, image);
                updateScene();
            };
            image.onerror = function() {
                window.alert("Invalid image: " + event.target.files[0].name);
            }
        };
        reader.readAsDataURL(event.target.files[0]);
    }
}

let lassoCoordinates = [];
function resetLasso() {
    lassoContext.clearRect(0, 0, lassoCanvas.width, lassoCanvas.height);
    lassoContext.fillStyle = "black";
    bindTextureFromImage(lassoTexture, lassoCanvas);
    updateScene();
}

/**
 * @author AxFab
 * @link https://stackoverflow.com/a/19048453/358116
 * @param canvas
 * @param event
 * @returns {{x: number, y: number}}
 */
function getMousePos(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}

function updateLasso(event) {
    let coordinates = getMousePos(glCanvas, event);
    console.log(coordinates);
    lassoCoordinates.push(coordinates);
    lassoContext.clearRect(0, 0, lassoCanvas.width, lassoCanvas.height);
    lassoContext.fillStyle = "black";
    lassoContext.beginPath();
    lassoContext.moveTo(lassoCoordinates[0].x, lassoCoordinates[0].y);
    for (let i = 1; i < lassoCoordinates.length; i++) {
        lassoContext.lineTo(lassoCoordinates[i].x, lassoCoordinates[i].y);
    }
    lassoContext.closePath();
    if (isMouseDown) {
        lassoContext.stroke();
        if (closeEnoughToCompleteLasso(lassoCoordinates)) {
            lassoContext.fill();
        }
    } else {
        if (closeEnoughToCompleteLasso(lassoCoordinates)) {
            lassoContext.stroke();
            lassoContext.fill();
        }
    }

    bindTextureFromImage(lassoTexture, lassoCanvas);
    updateScene();
}

gl.useProgram(imageProgram);
const programInfo = {
    program: imageProgram,
    attributeLocations: {
        vertexPosition: gl.getAttribLocation(imageProgram, "aVertexPosition"),
        textureCoord: gl.getAttribLocation(imageProgram, "aTextureCoord"),
    },
    uniformLocations: {
        projectionMatrix: gl.getUniformLocation(imageProgram, "uProjectionMatrix"),
        modelViewMatrix: gl.getUniformLocation(imageProgram, "uModelViewMatrix"),
        uBrightness: gl.getUniformLocation(imageProgram, "uBrightness"),
        uSampler: gl.getUniformLocation(imageProgram, "uSampler"),
        uLasso: gl.getUniformLocation(imageProgram, "uLasso"),
    },
};

function initBuffers(gl) {

    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    const positions = [
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    const textureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
    const textureCoordinates = [
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), gl.STATIC_DRAW);

    const indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    const indices = [
        0, 1, 2,
        0, 2, 3
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

    return {
        position: positionBuffer,
        textureCoord: textureCoordBuffer,
        indices: indexBuffer,
    };
}
const buffers = initBuffers(gl);

function drawScene(gl, programInfo, buffers, texture, lassoTexture) {
    gl.useProgram(programInfo.program);

    // vertexPosition attribute
    {
        const numComponents = 3;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        gl.vertexAttribPointer(
            programInfo.attributeLocations.vertexPosition,
            numComponents,
            type,
            normalize,
            stride,
            offset
        );
        gl.enableVertexAttribArray(programInfo.attributeLocations.vertexPosition);
    }

    // textureCoordinate attribute
    {
        const numComponents = 2;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureCoord);
        gl.vertexAttribPointer(
            programInfo.attributeLocations.textureCoord,
            numComponents,
            type,
            normalize,
            stride,
            offset
        );
        gl.enableVertexAttribArray(programInfo.attributeLocations.textureCoord);
    }

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        new Float32Array([
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ])
    );

    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        new Float32Array([
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ])
    );

    gl.uniform1f(programInfo.uniformLocations.uBrightness, brightnessMultiplier);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(programInfo.uniformLocations.uSampler, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, lassoTexture);
    gl.uniform1i(programInfo.uniformLocations.uLasso, 1);

    {
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
        const vertexCount = 6;
        const type = gl.UNSIGNED_SHORT;
        const offset = 0;
        gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }

}

function updateScene() {
    bindTextureFromImage(lassoTexture, lassoCanvas);
    drawScene(gl, programInfo, buffers, imageTexture, lassoTexture);
}

updateScene();

let isMouseDown = false;
glCanvas.addEventListener("mousedown", function (event) {
    isMouseDown |= event.which === 1;
    if (isMouseDown) {
        updateLasso(event);
    }
    event.preventDefault();
});
document.addEventListener("mousemove", function (event) {
    if (isMouseDown) {
        updateLasso(event);
    }
});
document.addEventListener("mouseup", function (event) {
    if (isMouseDown) {
        isMouseDown = false;
        updateLasso(event);
        if (event.which === 1) {
            lassoCoordinates = [];
        }
    }
});
