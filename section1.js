"use strict";

function problem1() {
    // Pull 10 digits out of a random number. Don't throw away randomness since regenerating it can be slow.
    let r = Math.random();
    let arr = [];
    for (let i = 0; i < 10; i++) {
        r *= 10;
        arr[i] = Math.floor(r);
        r = r % 1;
    }
    return arr;
}

function problem2(values) {
    let valuesCopy = values.slice();
    let sortedValues = valuesCopy.slice().sort().reverse();
    let topCount = 5;
    let minValue = sortedValues[topCount - 1];
    let indexes = [];
    for (let sortedIndex = 0; sortedIndex < sortedValues.length; sortedIndex++) {
        let value = sortedValues[sortedIndex];
        if (value < minValue) {
            break;
        }
        let unsortedIndex = valuesCopy.indexOf(value);
        valuesCopy[unsortedIndex] = false;
        indexes[sortedIndex] = unsortedIndex;
    }
    return indexes;
}

function problem3() {
    let fns = [];
    for (let i = 0; i < 10; i++) {
        fns[i] = function () {
            document.getElementById("problem1_3").innerText += "started task id " + i + "\n";
            // Running some asynchronous tasks here
            // ...
            document.getElementById("problem1_3").innerText += "completed task id " + i + "\n";
            console.log("completed task id " + i);
        };
    }

    function doNext() {
        if (!fns[doNext.idx]) {
            return;
        }
        fns[doNext.idx]();
        doNext.idx++;
        setTimeout(doNext, 1000);
    }

    doNext.idx = 0;

    doNext();
}

console.log("Problem 1:");
var randomArraySize10 = problem1();
console.log(randomArraySize10);
document.getElementById("problem1_1").innerText = JSON.stringify(randomArraySize10);

console.log("Problem 2:");
var sortedByValueIndices = problem2(randomArraySize10);
console.log(sortedByValueIndices);
document.getElementById("problem1_2").innerText = JSON.stringify(sortedByValueIndices);

problem3();
